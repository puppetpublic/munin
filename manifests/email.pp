# munin::email - munin node model for email servers

class munin::email inherits munin::as_root {
    file { '/etc/munin/plugin-conf.d/postfix':
        source  => 'puppet:///modules/munin/etc/munin/plugin-conf.d/postfix',
        require => Package['munin-node'],
    }

    munin::plugin { 'postfix_mailstats':  ensure => present }
    munin::plugin { 'postfix_mailvolume': ensure => present }
    munin::plugin { 'postfix_mailqueue':  ensure => present }
}
