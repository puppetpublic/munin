# Run munin as root user for accessing privileged log files
class munin::as_root inherits munin {
  $user = 'root'
  $group = 'root'
  File['/etc/munin/munin-node.conf'] {
    content => template('munin/munin-node.conf.erb'),
  }
}
