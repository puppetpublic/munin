# Munin software server class.
# NOTE: munin client module uses fact su_munin_server to tag munin conf files
# in storeconfigs. In the manifest for the munin server, use File <<| tag ==
# 'whatever' |>> to pickup storeconfigs files for munin on that server.

class munin::server {
    package { 'munin': ensure => installed }

    base::newsyslog::config { 'muninserver':
        frequency => 'monthly',
        directory => '/var/log/munin',
        log_owner => 'munin',
        log_group => 'munin',
        log_mode  => '0644',
        logs      => [  'munin-graph.log',
                        'munin-html.log',
                        'munin-limits.log',
                        'munin-update.log' ],
        save_num  => 7,
    }

}
