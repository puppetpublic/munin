# Munin client classes.

# Munin clients use storeconfigs for munin config fragments.  These are
# collected on the munin server.

# Uses 'su_munin_group' fact (if defined) to set munin group, then 'tries
# su_group' fact (if defined), then falls back to hostname.

#  To have a host use a different munin server, define the fact
#  'su_munin_server' with the fqdn of the host (or load balanced/shared dns
#  name).

class munin {
  package { 'munin-node': ensure => present; }

  case $::osfamily {
    'RedHat': {
      package {
        'perl-Crypt-DES':    ensure => present;
        'perl-Net-SNMP':     ensure => present;
        'perl-Net-Server':   ensure => present;
        'perl-Digest-SHA1':  ensure => present;
        'perl-Digest-HMAC':  ensure => present;
        'perl-Crypt-SSLeay': ensure => present;
        'perl-Socket6':      ensure => present;
        'perl-libwww-perl':  ensure => present;
      }
    }
    'Debian': {
      package { 'libcrypt-ssleay-perl': ensure => present }
    }
    default: { fail ("Unknown operating system '${::operatingsystem}'")}
  }

  # The iostat* plugins have moved to munin-plugins-extra as of wheezy.
  if (($::operatingsystem == 'debian') and ($::lsbdistcodename != 'squeeze')) {
      package {'munin-plugins-extra': ensure => present }
  }

  # Clients need to be reachable on port 4949 from the server.
  base::iptables::rule { 'munin':
    description => 'Allow queries from central Munin servers',
    source      => [ '171.67.17.23', '171.67.24.80', '171.67.217.112/28' ],
    protocol    => 'tcp',
    port        => 4949,
  }

  # Run munin as user 'munin' and group 'munin' by default.  If you change
  # these, you'll probably also have to override the log owners in the
  # newsyslog configuration.
  $user = 'munin'
  $group = 'munin'
  file {
    '/etc/munin':
      ensure  => directory,
      owner   => $user,
      group   => $group,
      require => Package['munin-node'];
    '/etc/munin/plugin-conf.d':
      ensure  => directory,
      owner   => $user,
      group   => $group,
      mode    => '0755',
      require => Package['munin-node'];
    '/etc/munin/plugin-conf.d/z-defaults':
      ensure  => directory,
      owner   => $user,
      group   => $group,
      source  => 'puppet:///modules/munin/etc/munin/plugin-conf.d/z-defaults';
    '/etc/munin/munin-node.conf':
      owner   => $user,
      group   => $group,
      content => template('munin/munin-node.conf.erb'),
      require => Package['munin-node'];
  }

  # Define the service and install the configuration file.
  service { 'munin-node':
    ensure     => running,
    hasstatus  => true,
    hasrestart => true,
    enable     => true,
    subscribe  => File['/etc/munin/munin-node.conf'];
  }

  # Log rotation.
  base::newsyslog::config { 'munin':
    restart   => 'run /usr/sbin/service-restart munin-node',
    directory => '/var/log/munin',
    log_owner => 'munin',
    log_group => 'munin',
    log_mode  => 644,
    logs      => [ 'munin-node.log' ],
    save_num  => 7,
  }

  # Plugin management.
  #
  # The idea here is to remove all plugins by default and add only the ones
  # that seem generally helpful.  When we were enabling all the plugins that
  # Munin turns on by default, we were generating tons and tons of graphs on
  # the central Munin server that no one cared about.  This errs a bit too
  # far the other direction, but does avoid that problem.
  #
  # We implement this by managing the /etc/munin/plugins directory and
  # enabling purge.  Then, only the links installed by munin::plugin
  # resources will be present.
  file { '/etc/munin/plugins':
    ensure  => directory,
    recurse => true,
    purge   => true,
    owner   => $user,
    group   => $group,
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }

  # A list of generally useful plugins worth running on each system.
  munin::plugin {
    'cpu':        ensure => present;
    'diskstats':  ensure => present;
    'iostat':     ensure => present;
    'iostat_ios': ensure => present;
    'if_eth0':    ensure => present, source => 'if_';
    'load':       ensure => present;
    'memory':     ensure => present;
    'vmstat':     ensure => present;
  }
  if $::virtual {
    munin::plugin { 'entropy': ensure => present }
  }

  # storeconfig file fragment for munin server
  # do all systems for now, including systems that are nonprod
  if ( $::su_munin_group != undef ) and ( $::su_munin_group != '' ) {
    $su_munin_groupname = $::su_munin_group
  }
  elsif ( $::su_group == undef ) or ( $::su_group == '' ) {
    $su_munin_groupname = $::hostname
  } else {
    $su_munin_groupname = $::su_group
  }
  # setup a sane default for munin config tag (use common munin server)
  if ( $su_munin_server == undef ) or ( $su_munin_server == '' ) {
    $this_munin_server = 'memory.stanford.edu'
  } else {
    $this_munin_server = $su_munin_server
  }
  @@file { "/etc/munin/munin-conf.d/${::hostname}.conf":
    content =>
      "[${su_munin_groupname};${::hostname}]\naddress ${::fqdn}\nuse_node_name yes",
    tag     => $this_munin_server;
  }

  # filter-syslog for munin
  file { '/etc/filter-syslog/munin':
    source  => 'puppet:///modules/munin/etc/filter-syslog/munin',
    require => File['/etc/filter-syslog'],
  }

}
