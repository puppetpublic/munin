# Enable or disable a munin-node plugin.  This definition should work for
# both Debian and Red Hat systems.  Example:
#
#   munin::plugin { 'slapd_bdb_cache_pages': source = 'slapd_' }
#   munin::plugin { 'postfix_mailstats': ensure => present }
#
# If source is set, create a symlink to it rather than to a file matching the
# name of the plugin.  This is used for the Munin plugins that support being
# run with mutiple options.

define munin::plugin(
    $ensure = present,
    $source = undef
) {
    if $source {
        $plugin = "/usr/share/munin/plugins/${source}"
    } else {
        $plugin = "/usr/share/munin/plugins/${name}"
    }

    file { "/etc/munin/plugins/${name}":
        ensure  => $ensure ? {
            present => link,
            default => $ensure,
        },
        target  => $plugin,
        require => Package['munin-node'],
        notify  => Service['munin-node'],
    }
}
